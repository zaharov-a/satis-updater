<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      31.10.2021
 * @copyright {Template_Description_Copyrights}
 */

return [
    'sshDir' => __DIR__ . '/../data/ssh',
    'satis'  => [
        'bin'          => '',
        'config'       => '',
        'output'       => __DIR__ . '/../public',
        'composerHome' => __DIR__ . '/../data/tmp',
    ],
    'tmp'    => __DIR__ . '/data/tmp',
    'logs'   => [
        'request' => __DIR__ . '/data/logs/request.json',
        'update'  => __DIR__ . '/data/logs/update.json',
    ],
];
