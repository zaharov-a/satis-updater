<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      12.10.2021
 * @copyright {Template_Description_Copyrights}
 */

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

function writeLog($file, $data)
{
    if (!$file) {
        return;
    }
    $data['time'] = date('c');
    file_put_contents(
        $file,
        json_encode($data, JSON_UNESCAPED_UNICODE) . PHP_EOL,
        FILE_APPEND
    );
}

$config = require __DIR__ . '/../configs/config.cfg.php';

$sshDir = $config['sshDir'];

$sshKey = "{$sshDir}/id_rsa";
$sshKnownHosts = '-o UserKnownHostsFile=' . $sshDir . '/known_hosts';
$satisCfg = $config['satis'];

$extraArgs = '';
if (!isset($_GET['all'])) {
    $json = file_get_contents("php://input");

    writeLog($config['logs']['request'] ?? null, ['request' => $json]);

    if (!$json) {
        writeLog($config['logs']['update'] ?? null, ['error' => 'Пустой запрос']);
        exit(1);
    }
    $data = json_decode($json);

    $repositories = json_decode(file_get_contents($satisCfg['config']))->repositories;

    $repositoryUrl = '';
    foreach ($repositories as $repository) {
        if (false !== strpos($repository->url, $data->repository->full_name)) {
            $repositoryUrl = $repository->url;
        }
    }

    if (!$repositoryUrl) {
        writeLog(
            $config['logs']['update'] ?? null,
            ['error' => "Неизвестный репозиторий: {$data->repository->full_name}"]
        );
        exit(2);
    }

    $extraArgs = "--repository-url {$repositoryUrl}";
}

$command = "COMPOSER_HOME={$satisCfg['composerHome']} GIT_SSH_COMMAND=\"ssh -i {$sshKey} {$sshKnownHosts}\" {$satisCfg['bin']} build {$extraArgs}  {$satisCfg['config']} {$satisCfg['output']}";

$descriptorspec = [
    0 => ["pipe", "r"], // stdin - канал, из которого дочерний процесс будет читать
    1 => ["pipe", "w"], // stdout - канал, в который дочерний процесс будет записывать
    2 => ["pipe", "w"], // stderr - файл для записи
];

$process = proc_open($command, $descriptorspec, $pipes);

if (is_resource($process)) {
    // $result = shell_exec($command);

    $result = stream_get_contents($pipes[1]);
    $error  = stream_get_contents($pipes[2]);

    $exitCode = proc_close($process);

    writeLog(
        $config['logs']['update'] ?? null,
        [
            'command' => $command,
            'result'  => $result,
            'error'   => $error,
            'code'    => $exitCode,
        ]
    );
} else {
    writeLog(
        $config['logs']['update'] ?? null,
        [
            'command' => $command,
            'error'   => 'Не удалось запустить процесс satis',
        ]
    );
}


